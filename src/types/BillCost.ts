type BillCost = {
  id: number
  type: string // 'Electricity bill' | 'Water bill' | 'Rent bill'| 'other'
  date: string
  time: string
  total: number
  other: string
}

export { type BillCost }
