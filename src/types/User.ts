type Gender = 'male' | 'female' | 'others'
type Role = 'manager' | 'staff'
type User = {
    id:number
    email:string
    password:string
    fullname:string
    gender:Gender
    role:Role
}

export type {Gender,Role,User}