import type { User } from "./User";

type CheckIN = {
    id: number;
    user: User
    date: string,
    in: string;
    out: string;
    hour: number;
    total: number;

}

export { type CheckIN }