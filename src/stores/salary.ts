import { ref, nextTick } from 'vue'
import { defineStore } from 'pinia'
import type { Salary } from '@/types/Salary'
import { useAuthStore } from './auth'
import { useUserStore } from './user'
import { useCheckInStore } from './checkIn'
import type { CheckIN } from '@/types/CheckIN'


export const useSalaryStore = defineStore('salary', () => {
  let lastIndex = 7
  let editedIndex = -1
  const dialog = ref(false)
  const reportSalaryDialog = ref(false)
  const search = ref('')
  const initSalary: Salary = {
    idSalary: -1,
    userId: -1,
    month: '',
    status: 'Not Paid',
    salarybaht: 25000
  }
  const userStore = useUserStore()
  const authStore = useAuthStore()
  const checkuserStore = useCheckInStore()
  const salaryitem = ref<Salary>()
  const editedsalary = ref<Salary>(JSON.parse(JSON.stringify(initSalary)))
  const salarys = ref<Salary[]>([
    { userId: 1, idSalary: 1, month: 'January', status: ' Not Paid ', salarybaht: 25000 },
    { userId: 2, idSalary: 2, month: 'January', status: ' Not Paid ', salarybaht: 15000 },
    { userId: 3, idSalary: 3, month: 'January', status: ' Not Paid ', salarybaht: 15000 },
    { userId: 4, idSalary: 4, month: 'January', status: ' Not Paid ', salarybaht: 15000 },
    { userId: 5, idSalary: 5, month: 'January', status: ' Not Paid ', salarybaht: 15000 },
    { userId: 6, idSalary: 6, month: 'January', status: ' Not Paid ', salarybaht: 15000 }
  ])
  const editedsalarys = ref<Salary[]>([])
  function editItem(item: Salary) {
    const indextools = checkuserStore.checkInOuts.findIndex((item1) => item1.user.id === item.idSalary)
    console.log(indextools, "indextool");

    console.log(item);
    // editedSalry()
    const index = userStore.users.findIndex((item1) => item1.id === item.idSalary)
    // salarys.value[index].user=userStore.users[index]
    item.user = userStore.users[index]
    // item.checkin=checkuserStore.checkInOuts[index]
    addcheckin(item)
    editedIndex = salarys.value.indexOf(item)
    editedsalary.value = item
    console.log(item.checkin, "checkin");
    // item.user!.fullname=userStore.users[index].fullname
    editedsalary.value = Object.assign({}, editedsalary.value)
    console.log(index);
    console.log(editedsalary);

    editedsalarys.value.unshift(editedsalary.value)
    console.log(editedsalarys.value.length);
    // console.log("fullname",editedsalarys.value[index].user!.fullname);
    // editedSalarys()
    openDialog1()
  }
  function editedSalarys() {
    for (let index = 0; index < editedsalarys.value.length; index++) {
      //  if(editedsalarys.value[index].userId==userStore.users[index].id){
      //   editedsalarys.value[index].user!.fullname=userStore.users[index].fullname
      //   console.log("fullname",editedsalarys.value[index].user!.fullname);
      //  }
      editedsalarys.value[index].user!.fullname = userStore.users[index].fullname
      console.log("fullname", editedsalarys.value[index].user!.fullname);
    }
  }
  function editedSalry() {
    for (let index = 0; index < salarys.value.length; index++) {
      salarys.value[index].user = userStore.users[index]
    }
    console.log("user");

  }
  function closeDialog() {
    clearSalary()
    dialog.value = false
    nextTick(() => {
      editedsalary.value = Object.assign({}, initSalary)
      editedIndex = -1
    })
  }
  function openDialog1() {
    dialog.value = !dialog.value
  }
  function clearSalary() {
    editedsalarys.value = []
    editedsalary.value = Object.assign({}, initSalary)
   console.log('CLEAR');
   


  }
  function addcheckin(s: Salary) {
    for (let index = 0; index < checkuserStore.checkInOuts.length; index++) {
      if (s.userId == checkuserStore.checkInOuts[index].user.id) {
        s.checkin = checkuserStore.checkInOuts?.[index]
      }
    }
    console.log("addcheckin", s.checkin);
  }
  // function save() {
  //   if (editedIndex > -1) {
  //     Object.assign(salarys.value[editedIndex], editedsalary.value);
  //   } else {
  //     editedsalary.value.idSalary = lastIndex++;
  //     salarys.value.push(editedsalary.value);
  //   }
  //   editedIndex = -1;
  //   closeDialog();
  // }

  function save() {
    const index = salarys.value.findIndex(salary => salary.idSalary === editedsalary.value.idSalary);
    if (index > -1) {
      salarys.value[index] = { ...salarys.value[index], ...editedsalary.value };
    } else {
      editedsalary.value.idSalary = lastIndex++;
      salarys.value.push(editedsalary.value);
    }
    closeDialog();
  }

  function openReportSalaryDialog() {
    reportSalaryDialog.value = !reportSalaryDialog.value
  }




  return {
    editedsalary,
    salarys,
    editItem,
    dialog,
    closeDialog,
    save,
    lastIndex,
    editedIndex,
    openReportSalaryDialog,
    reportSalaryDialog,
    editedsalarys,
    salaryitem,
    clearSalary,

    search
  }
})
