// import { nextTick, ref } from 'vue'
// import { defineStore } from 'pinia'
// import type { BillCost } from '@/types/BillCost'

// export const useBillCostStore = defineStore('billCost', () => {
//   const dialog = ref(false)
//   const dialogDelete = ref(false)
//   let editedIndex = -1
//   const search = ref('')
//   let lastIndex = 7
//   const initBillCost:BillCost = {
//     id: -1,
//     type: '',
//     date: '',
//     time: '',
//     total: 0,
//     other: '',
//     }

//   const editedbillCost = ref<BillCost>(JSON.parse(JSON.stringify (initBillCost)))
//   const billCosts = ref<BillCost[]>([
//     { id: 1, type: 'Electricity bill', date:'2023-12-5',time: '12:53:30', total:5000 ,other: ''},
//     { id: 2, type: 'Water bill', date:'2023-12-5',time: '12:53:30',total:1000 ,other: ''},
//     { id: 3, type: 'Rent bill', date:'2023-12-5',time: '12:53:30', total:10000 ,other: ''},
//     { id: 4, type: 'Electricity bill', date:'2024-01-5',time: '12:53:30',total:5500 ,other: ''},
//     { id: 5, type: 'Water bill', date:'2024-01-5',time: '12:53:30', total:1000 ,other: ''},
//     { id: 6, type: 'Rent bill', date:'2024-01-5',time: '12:53:30', total:10000 ,other: ''},
//     { id: 7, type: 'Rent bill', date:'2024-01-5',time: '12:53:30', total:10000 ,other: ''},
//   ])
//   function openDialog(){
//     dialog.value = true
//     nextTick(() => {
//         editedbillCost.value = Object.assign({}, initBillCost)
//       editedIndex = -1
//     })
//   }
//   // function save(){
//   //   if (editedbillCost.value.id > -1){
//   //    Object.assign(billCosts.value[editedIndex], editedbillCost.value)
//   //  } else {
//   //    editedbillCost.value.id = lastIndex++
//   //    billCosts.value.push(editedbillCost.value)
//   //   }
//   //   closeDialog()
//   //  }

//    function save() {
//     if (editedbillCost.value.id > -1) {
//       Object.assign(billCosts.value[editedIndex], editedbillCost.value);
//     } else {
//       editedbillCost.value.id = lastIndex++;
//       billCosts.value.unshift(editedbillCost.value);
//     }
//     closeDialog();
//   }

//  function closeDialog(){
//   dialog.value = false
//   nextTick(() => {
//       editedbillCost.value = Object.assign({}, initBillCost)
//     editedIndex = -1
//   })
// }
//     function closeDelete() {
//       dialogDelete.value = false
//       nextTick(() => {
//         editedbillCost.value = Object.assign({}, initBillCost)
//       })
//     }

//   function deleteItem(item:BillCost) {
//     editedIndex = billCosts.value.indexOf(item)
//     editedbillCost.value = Object.assign({}, item)
//           dialogDelete.value = true

//   }
//   function  deleteItemConfirm () {
//       // delete item from List
//       billCosts.value.splice(editedIndex, 1)
//       closeDelete()
//     }

//   return {
//     billCosts,
//     openDialog,
//     dialog,
//     editedbillCost,
//     save,
//     closeDialog,
//     deleteItem,
//     closeDelete,
//     dialogDelete,
//     deleteItemConfirm,
//     search

//   }
// })

////ทดสอบ////
import { ref, computed, nextTick } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import billcostService from '@/services/billcost'
import type { BillCost } from '@/types/BillCost'
import type { VForm } from 'vuetify/components'

export const useBillCostStore = defineStore('billcost', () => {
  const loadingStore = useLoadingStore()
  const billCosts = ref<BillCost[]>([])
  const search = ref('')
  const dialog = ref(false)
  const dialogDelete = ref(false)
  let editedIndex = -1
  const refForm = ref<VForm | null>(null)
  let lastIndex = 7
  const initBillCost: BillCost = {
    id: -1,
    type: '',
    date: '',
    time: '',
    total: 0,
    other: ''
  }
  const editedbillCost = ref<BillCost>(JSON.parse(JSON.stringify(initBillCost)))

  async function save() {
    console.log('2')
    const { valid } = await refForm.value!.validate()
    console.log('Validation result:', valid);
    if (!valid) return
    console.log('Form is not valid. Exiting save function.');
    closeDialog()
    await saveBillcost(editedbillCost.value)
  }

  function openDialog() {
    dialog.value = true
    nextTick(() => {
      editedbillCost.value = Object.assign({}, initBillCost)
      editedIndex = -1
    })
  }
  function closeDialog() {
    dialog.value = false
    nextTick(() => {
      editedbillCost.value = Object.assign({}, initBillCost)
      editedIndex = -1
    })
  }

  function closeDelete() {
    dialogDelete.value = false
    nextTick(() => {
      editedbillCost.value = Object.assign({}, initBillCost)
    })
  }

  function deleteItem(item: BillCost) {
    editedIndex = billCosts.value.indexOf(item)
    editedbillCost.value = Object.assign({}, item)
    dialogDelete.value = true
  }
  function deleteItemConfirm() {
    // delete item from List
    billCosts.value.splice(editedIndex, 1)
    closeDelete()
  }

  async function getBillcost(id: number) {
    loadingStore.doLoad()
    const res = await billcostService.getBillcost(id)
    billCosts.value = res.data
    loadingStore.finish()
  }

  async function getBillcosts() {
    loadingStore.doLoad()
    const res = await billcostService.getBillcosts()
    billCosts.value = res.data
    loadingStore.finish()
  }

  async function saveBillcost(billcost: BillCost) {
    loadingStore.doLoad()
    if (billcost.id < 0) {
      // add new
      console.log('Post' + JSON.stringify(billcost))
      const res = await billcostService.addBillcost(billcost)
    } else {
      // update
      console.log('Patch' + JSON.stringify(billcost))
      const res = await billcostService.updateBillcost(billcost)
    }
    await getBillcosts()
    loadingStore.finish()
  }

  async function deleteBillCost(billcost: BillCost) {
    loadingStore.doLoad()
    const res = await billcostService.delBillcost(billcost)
    await getBillcosts()
    loadingStore.finish()
  }

  return {
    billCosts,
    getBillcosts,
    saveBillcost,
    deleteBillCost,
    search,
    closeDialog,
    editedbillCost,
    openDialog,
    dialog,
    dialogDelete,
    closeDelete,
    deleteItem,
    deleteItemConfirm,
    save
  }
})
