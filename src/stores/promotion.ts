import { ref, computed, nextTick } from 'vue'
import { defineStore } from 'pinia'
import type { Promotion } from '@/types/Promotion'
import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Receipt } from '@/types/Receipt'
import { useProductStore } from './product'
import { useReceiptStore } from './receipt'

export const usePromotionStore = defineStore('promotion', () => {
  const dialog = ref(false)
  const dialogType = ref(false)
  const dialogType2 = ref(false)
  const dialogType3 = ref(false)
  const dialogDelete = ref(false)
  const product = ref()
  const search = ref('')
  const form = ref(false)
  const currentPromotion = ref<Promotion | null>()
  const disMax = ref(0)
  const receiptStore = useReceiptStore()
  
  let editedIndex = -1

  const initPromotion: Promotion = {
    id: -1,
    name: '',
    con: '',
    start: '',
    end: '',
    status: false,
    discount: 0,
    unit: 0,
    product: undefined,
    price: 0
  }
  const editedPromotion = ref<Promotion>(JSON.parse(JSON.stringify(initPromotion)))
  const promotions = ref<Promotion[]>([
    {
      id: 1,
      name: 'มาคนเดียวทำไมซื้อ3แก้วอะ',
      con: 'ถ้าซื้อครบ 3 แก้ว ลดราคาทั้งหมด 10%',
      start: '01-01-2004',
      end: '30-01-2004',
      status: true,
      discount: 0.1,
      unit: 3,
      price: 0
    },
    {
      id: 2,
      name: 'มาคนเดียวทำไมซื้อ5แก้วอะ',
      con: 'ถ้าซื้อครบ 5 แก้ว ลดราคาทั้งหมด 20%',
      start: '01-01-2004',
      end: '30-01-2004',
      status: true,
      discount: 0.2,
      unit: 5,
      price: 0
    },
    {
      id: 3,
      name: 'มอคค่าของโคตรดี',
      con: 'มอคค่าราคา 40 บาท',
      start: '01-01-2004',
      end: '30-01-2004',
      status: true,
      discount: 5,
      unit: 0,
      product: 'มอคค่า',
      price: 40
    },
    {
      id: 4,
      name: 'เซ็ทปีใหม่',
      con: 'ซื้อเป็นเซ็ท ลาเต้,ครัวซอง ราคา 70 บาท',
      start: '01-01-2004',
      end: '30-01-2004',
      status: true,
      discount: 10,
      unit: 0,
      product: "ลาเต้,ครัวซอง",
      price: 70
    },{
      id: 5,
      name: 'มาคนเดียวทำไมซื้อ10แก้วอะ',
      con: 'ถ้าซื้อครบ 10 แก้ว ลดราคาทั้งหมด 30%',
      start: '01-01-2004',
      end: '30-01-2004',
      status: true,
      discount: 0.3,
      unit: 10,
      price: 0
    },{
      id: 6,
      name: 'มาคนเดียวทำไมซื้อ6แก้วอะ',
      con: 'ถ้าซื้อครบ 6 แก้ว ลดราคาทั้งหมด 30%',
      start: '01-01-2004',
      end: '30-01-2004',
      status: true,
      discount: 0.21,
      unit: 6,
      price: 0
    },{
      id: 7,
      name: 'มาคนเดียวทำไมซื้อ10แก้วอะ',
      con: 'ถ้าซื้อครบ 10 แก้ว ลดราคาทั้งหมด 30%',
      start: '01-01-2004',
      end: '30-01-2004',
      status: true,
      discount: 0.3,
      unit: 10,
      price: 0
    }
    
  ])

  let lastId = promotions.value.length + 1
  function promotionUnit(r: ReceiptItem[]) {
    promotions.value.sort((a, b) => b.unit - a.unit)
    for (let index = 0; index < promotions.value.length; index++) {
      let count = 0
      if (promotions.value[index].unit != 0 && promotions.value[index].status == true) {
        if (r != undefined) {
          for (let index = 0; index < r.length; index++) {
            count += r[index].unit
          }
          if (count >= promotions.value[index].unit) {
            const dis = ref(0)
            for(const i of r){
              dis.value += i.price*i.unit
            }
            dis.value = dis.value*promotions.value[index].discount
            checkDisMax(dis.value,promotions.value[index])
            dis.value = 0
            return
          }
        }
      }
    }
  }
  function checkDisMax(d: number,p:Promotion) {
    if (d > disMax.value) {
      disMax.value = d
    currentPromotion.value = p
    receiptStore.receipt.promotion = currentPromotion.value
    }if(disMax.value == 0){
      currentPromotion.value = null
      receiptStore.receipt.promotion = undefined
    }
    receiptStore.receipt.promotionDiscount = disMax.value
    
  }
  function clearPromo() {
    currentPromotion.value = null
  }

  function deletePromotion() {
    console.log(editedIndex)
    promotions.value.splice(editedIndex, 1)
    dialogDelete.value = false
  }

  function closeDialog() {
    dialogDelete.value = false
    nextTick(() => {
      editedPromotion.value = Object.assign({}, initPromotion)
      editedIndex = -1
    })
  }

  function openDialogDelete(p: Promotion) {
    editedIndex = promotions.value.indexOf(p)
    editedPromotion.value = Object.assign({}, p)
    dialogDelete.value = true
  }

  function editPromotion(p: Promotion) {
    editedIndex = promotions.value.indexOf(p)
    editedPromotion.value = Object.assign({}, p)
    if(editedPromotion.value.unit!=0){
      dialog.value = true
      return
    }if(typeof editedPromotion.value.product  ==="string"){
      product.value = editedPromotion.value.product
      dialogType2.value = true
      return
    }else{
      dialogType3.value = true
      product.value = editedPromotion.value.product
    }
  }

  function addPromotion() {
    dialog.value = true
  }

  function save() {
    if (editedPromotion.value.id > -1) {
      Object.assign(promotions.value[editedIndex], editedPromotion.value)
      closeDialogAdd()
      closeDialog()
    } else {
      editedPromotion.value.id = lastId++
      promotions.value.push(editedPromotion.value)
      closeDialogAdd()
    }
  }
  function closeDialogAdd() {
    dialog.value = false

    nextTick(() => {
      editedPromotion.value = Object.assign({}, initPromotion)
      editedIndex = -1
    })
  }
  function checkProductdis(r: ReceiptItem[]) {
    const dis = ref(0)
    promotions.value.sort((a, b) => b.discount - a.discount)
    for (const p of promotions.value) {
      if (p.product != null && p.product != undefined && typeof p.product === "string") {
        for (let i = 0; i < r.length; i++) {
          if (p.product === r[i].product?.name) {
             dis.value += p.discount*r[i].unit
          }
        }
        checkDisMax(dis.value,p)
        dis.value = 0
      }
    }
  }
  function checkProductSetdis(r: ReceiptItem[]) {
    promotions.value.sort((a, b) => b.price - a.price)
    for (const p of promotions.value) {
      if (p.product != null && p.product != undefined) {
        const productListName = ref<string[]>([])
        productListName.value = p.product.split(",")
        const arraySet: number[] = Array(productListName.value.length).fill(0);
        for (let i = 0; i < r.length; i++) {
           for (let j = 0; j < productListName.value.length; j++) {
              if(r[i].product?.name === productListName.value[j]){
                arraySet[j] = r[i].unit
              }
            }
        }
        if(Math.min(...arraySet)!=0){
          checkDisMax(p.discount*Math.min(...arraySet),p)
        }
      }
    }
  }
  function cleardialog(){
    product.value = ''
    editedPromotion.value = Object.assign({}, initPromotion)
      editedIndex = -1
  }
  return {
    promotions,
    currentPromotion,
    disMax,
    dialog,
    dialogDelete,
    form,
    editedPromotion,
    dialogType,
    dialogType2,
    dialogType3,
    product,
    editedIndex,
    search,
    promotionUnit,
    clearPromo,
    deletePromotion,
    closeDialog,
    openDialogDelete,
    editPromotion,
    addPromotion,
    save,
    checkProductdis,
    checkProductSetdis,
    cleardialog
  }
})
