import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { CheckIN } from '@/types/CheckIN'

import { useAuthStore } from './auth'
import { useUserStore } from './user'
import type { User } from '@/types/User'

export const useCheckInStore = defineStore('checkIn', () => {
  const showCheckInButton = ref(true)
  const dialogVisible = ref(false)
  const checkoneday = ref(false);
  const authStore = useAuthStore()
  const userStore = useUserStore()
  const date = ref()
  const formattedDate = ref<string>('')
  const textDate = ref()
  const initCheckIn: CheckIN = {
    id: 0,
    user: authStore.currentUser!,
    in: '',
    out: '',
    date: '',
    hour: 0,
    total: 0
  }
  const checkInOuts = ref<CheckIN[]>([])
  let lastId = 1
  const editedCheckIn = ref<CheckIN>(JSON.parse(JSON.stringify(initCheckIn)))
  function checkInTime() {
    date.value = new Date()
    formattedDate.value = date.value.toLocaleString('en-US', {
      day: 'numeric',
      month: 'numeric',
      year: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
      hour12: false // 24 ชั่วโมง
    })
    const tt = ref(formattedDate.value.split(','))
    console.log(tt.value[0])

    editedCheckIn.value.id = lastId++
    editedCheckIn.value.in = tt.value[1]
    editedCheckIn.value.date = tt.value[0]
    checkInOuts.value.unshift(editedCheckIn.value)
    console.log(editedCheckIn.value)
  }
  function checkOutTime() {
    date.value = new Date()
    formattedDate.value = date.value.toLocaleString('en-US', {
      day: 'numeric',
      month: 'numeric',
      year: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
      hour12: false, // 24 ชั่วโมง
    });

    const tt = ref(formattedDate.value.split(','))

    const outTime = new Date(date.value);
    outTime.setHours(outTime.getHours() + 8); // บวก ชั่วโมง 8 ชั่วโมง
    
    const outTimeString = outTime.toLocaleTimeString('en-US', {
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit',
      hour12: false,
    });
    
    checkInOuts.value[0].out = outTimeString
    const inputParts = checkInOuts.value[0].in.split(':');
    const inputHours = parseInt(inputParts[0], 10);
    const inputMins = parseInt(inputParts[1], 10);
    const outputParts = checkInOuts.value[0].out.split(':');
    const outputHours = parseInt(outputParts[0], 10);
    const outputMins = parseInt(outputParts[1], 10);
    // ตรวจสอบว่า TimeOut มีค่าน้อยกว่า TimeIn หรือไม่
    let adjustedOutputHours = outputHours;
    if (outputHours < inputHours || (outputHours === inputHours && outputMins < inputMins)) {
      adjustedOutputHours += 24; // เพิ่ม 24 ชั่วโมง
    }
    // คำนวณชั่วโมง
    const totalHours = adjustedOutputHours - inputHours;
    checkInOuts.value[0].hour = totalHours;
    // คำนวณค่าแรง
    checkInOuts.value[0].total = totalHours * 50;
    editedCheckIn.value = initCheckIn;
  }
    

//     const tt = ref(formattedDate.value.split(','))

//     const outTime = new Date(date.value);
//     outTime.setHours(outTime.getHours() + 8); // บวก ชั่วโมง 8 ชั่วโมง

//    const outTimeString = outTime.toLocaleTimeString('en-US', {
//     hour: '2-digit',
//     minute: '2-digit',
//     second: '2-digit',
//     hour12: false,
//   });
//     checkInOuts.value[0].out = outTimeString
//     // checkInOuts.value[0].out = '15:00:00'
//     const inputParts = checkInOuts.value[0].in.split(':');
// const inputHours = parseInt(inputParts[0], 10);
// const inputmins = parseInt(inputParts[1], 10);
// const outputParts = checkInOuts.value[0].out.split(':');
// const outputmins = parseInt(outputParts[1], 10);
// const outputHours = parseInt(outputParts[0], 10);

//     // แปลงระหว่างวินาทีเป็นชั่วโมง, นาที, และวินาที
//     const mins = ref(0)
//     if (inputmins > outputmins) {
//       mins.value = 1
//     }
//     checkInOuts.value[0].hour = outputHours - inputHours - mins.value
//     checkInOuts.value[0].total = checkInOuts.value[0].hour * 50
//     editedCheckIn.value = initCheckIn
//   }

  function chkUser(){
    const n = userStore.checkAuth(authStore.email, authStore.password)
    if (n == null) {
      console.log("null");
    } else {
      console.log('nice');
    }
  }
  return { editedCheckIn, checkInTime, checkInOuts, checkOutTime, chkUser,showCheckInButton,dialogVisible,checkoneday }
})
