import type { BillCost } from '@/types/BillCost'
import http from './http'

function addBillcost(billcost: BillCost) {
  return http.post('/billcosts', billcost)
}

function updateBillcost(billcost: BillCost) {
  return http.patch(`/billcosts/${billcost.id}`, billcost)
}

function delBillcost(billcost: BillCost) {
  return http.delete(`/billcosts/${billcost.id}`)
}

function getBillcost(id: number) {
  return http.delete(`/billcosts/${id}`)
}

function getBillcosts() {
  return http.get('/billcosts')
}

export default { addBillcost, updateBillcost, delBillcost, getBillcost, getBillcosts }
